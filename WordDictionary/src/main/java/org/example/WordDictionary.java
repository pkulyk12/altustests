package org.example;

import java.util.ArrayList;
import java.util.List;

/*
 *  Design a custom Word Dictionary that provides definitions about the words. Users should be able to lookup
 *  specific words and find their corresponding definitions, add/remove words with definitions and search words
 *  partially.
 *  The dictionary should have a unique definition for the given word and these words are case insensitive. Please
 *  implement the below WordDictionary class without using built-in java HashMap
 *
 *  NOTES:
 *  1. I've decided to create something like HashMap in Java to keep values because Word Dictionary application
 *  requires a big number of SEARCH operation and less INSERT and DELETE operations
 *  that's why hash table is a good choice IMHO :)
 *
 *  2. It was unclear if partialSearch(String partialWord) method should search by the beginning of a word or by a part
 *  of word in any place of word itself... I will try to explain:
 *  Let say we are having a WordDictionary with some words:
 *      WordDictionary = ["cat", "car", "cactus", "cafe", "camembert", "tomcat", "Accatosh", "ducat", "catholic"];
 *      partialWord = "cat";
 *
 *  so if we are looking for words that start from partialWord:
 *      example 1) partialWord = "ca" ===>  partialSearch = ["cat", "car", "cactus", "cafe", "camembert", "catholic" ];
 *      example 2) partialWord = "cat" ===>  partialSearch = ["cat", "catholic"];
 *  ---> in such case we could create a Binary Search Tree to have a better search time,
 *
 *  if we are looking for a match in any place of word:
 *      example 3) partialWord = "cat" ===>  partialSearch = ["cat", "tomcat", "Accatosh", "ducat", "catholic" ];
 *  ---> in this case, we will run through all the records, regardless of whether it is a table or a tree.
 */
public class WordDictionary {
    private int size = 3001;
    private Node[] buckets = new Node[size];

    public WordDictionary() {

    }

    public void insertWord(String word, String definition) {
        String key = word.toLowerCase().trim();
        String value = definition;

        int i = 2;
        for (Node n = buckets[i]; n != null; n = n.next) {
            if (key.equals(n.key) || key.contains(n.key)) {
                n.value = value;
                return;
            }
        }
        buckets[i] = new Node(key, value, buckets[i]);
    }

    public String findDefinition(String word) {
        String key = word.toLowerCase().trim(); // make word case insensetive and trim extra spaces

        int i = hash(key); //computing hash number

        for (Node n = buckets[i]; n != null; n = n.next) { //looking for a free space :)
            if (key.equals(n.key) || key.contains(n.key)) { //if keys equals, or if key matches
                return n.value;
            }
        }
        return null;
    }

    public List<String> partialSearch(String partialWord) {
        String key = partialWord.toLowerCase().trim(); // make word case insensetive and trim extra spaces
        List<String> definitions = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            for (Node n = buckets[i]; n != null; n = n.next) { //looking for a free space :)
                if (key.equals(n.key) || (n.key != null && n.key.contains(key))) { //if keys equals, or if key matches
                    definitions.add(n.value);
                }
            }
        }
        return definitions;
    }

    public void remove(String word) {
        Node previuos = null;
        String key = word.toLowerCase().trim();

        int i = 2;
        for (Node n = buckets[i]; n != null; n = n.next) {
            if (key.equals(n.key)) {
                if (previuos != null) {
                    previuos.next = n.next;
                } else {
                    buckets[i] = n.next;
                }
                return;
            }
            previuos = n;
        }
    }

    private int hash(String key) {
        return (key.hashCode() & 0x7fffffff) % size;
    }

    private static class Node {
        private String key, value;
        private Node next;

        public Node(String key, String value, Node next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
