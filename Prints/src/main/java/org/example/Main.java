package org.example;

/*
 * The following program prints,
 * a) “Happy Face” text when you feed a child with “Ice Cream” and
 * b) “Angry Face” text when you feed a child with “Salad” and
 * c) “Normal Face” text when you feed a child with “Milk”
 * d) "Error Face" text if you do not feed child with either "Ice Cream", "Salad" or "Milk
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Feed Child With Ice Cream, Salad or Milk To See Child's Face Reaction : ");
        String food = reader.readLine();

        printIfElse(food);
        printSwitchCase(food);
        printTernary(food);
        printHashMap(food);
    }

    //the most basic realization
    //easy to implement and pretty easy to read
    //unless big number of options and huge conditions
    private static void printIfElse(String food) {
        System.out.print("[if-else]: ");
        if (food.equalsIgnoreCase("Ice Cream")) {
            System.out.println("Happy" + " " + "Face");
        } else if (food.equalsIgnoreCase("Salad")) {
            System.out.println("Angry" + " " + "Face");
        } else if (food.equalsIgnoreCase("Milk")) {
            System.out.println("Normal" + " " + "Face");
        } else {
            System.out.println("Error" + " " + "Face");
        }
    }

    //pretty easy to read
    //also easy to mess up when missed "break;" keyword :)
    //a bit more readable than "if-else" implementation when there are a lot of cases
    private static void printSwitchCase(String food) {
        System.out.print("[switch-case]: ");
        switch (food.toLowerCase()) {
            case "ice cream":
                System.out.println("Happy" + " " + "Face");
                break;
            case "salad":
                System.out.println("Angry" + " " + "Face");
                break;
            case "milk":
                System.out.println("Normal" + " " + "Face");
                break;
            default:
                System.out.println("Error" + " " + "Face");
        }
    }

    //for fun
    private static void printHashMap(String food) {
        System.out.print("\n[hash-map O_o (omg)]: ");
        Map<String, String> map = new HashMap<>();
        map.put("ice cream", "Happy" + " " + "Face");
        map.put("salad", "Angry" + " " + "Face");
        map.put("milk", "Normal" + " " + "Face");
//        map.put("", "Error" + " " + "Face");
//        map.put("null", "You shall not pass!");


        String message = map.get(food.toLowerCase());
        if (message != null) {
            System.out.println(message);
        } else {
            System.out.println("Error" + " " + "Face");
        }
    }

    //easy to operate with simple conditions
    //otherwise it is very difficult to track the final result
    private static void printTernary(String food) {
        System.out.print("[ternary]: ");
        System.out.println(
                food.equalsIgnoreCase("Ice Cream") ? "Happy" + " " + "Face" :
                        food.equalsIgnoreCase("Salad") ? "Angry" + " " + "Face" :
                                food.equalsIgnoreCase("Milk") ? "Normal" + " " + "Face" : "Error" + " " + "Face"
        );
    }

}