package org.example;

/*
 *  Implement the main method below with instructions to guide the robot through any maze using the Robot&#39;s
 *  methods explained above. The program should end once the robot has successfully completed the maze. Try to
 *  code your solution in as few lines as possible.
 */
public class Main {

    public static void main(String[] args) {
        Robot robot = new Robot();

        while (!robot.hasExited()) {                        // until got exit
            if (robot.isPathClear()) {                      // can move forward?
                robot.moveForward();                        // if yes then go
            } else {                                        // otherwise
                robot.turnLeft();                           //
                robot.turnLeft();                           // turn left x 3
                robot.turnLeft();                           //
                if (robot.isPathClear()) {                  // can move forward?
                    robot.moveForward();                    // if yes then go
                } else {                                    // otherwise
                    robot.turnLeft();                       //
                    robot.turnLeft();                       // turn left 2x
                }
            }
            robot.turnLeft();                               // turn left
        }
    }

}